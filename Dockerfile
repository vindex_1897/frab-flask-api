FROM python:2.7
LABEL maintainer="neil@invento.io"

COPY . /app
WORKDIR /app

RUN pip install pipenv

RUN pipenv install --deploy --ignore-pipfile

# RUN pipenv run python manage.py db init
# RUN pipenv run python manage.py db migrate
# RUN pipenv run python manage.py db upgrade

# CMD ["pipenv", "run", "python", "run.py"]

RUN chmod u+x ./entrypoint.sh
# ENTRYPOINT ["./entrypoint.sh"]