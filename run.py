# /run.py
import os

# from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate
from dotenv import load_dotenv

#loading env vars
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

from src.app import create_app

if __name__ == '__main__':
  api = create_app(os.environ.get('ENVIRONMENT'))

  # run app
  # api.run(host='0.0.0.0',port=38000) 
  api.run(host=os.environ.get('API_HOST'), port=os.environ.get('API_PORT'))
  # api.run() 
