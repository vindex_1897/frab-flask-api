from flask import Flask
from flask_cors import CORS
# from flask_wtf.csrf import CSRFProtect

from config import app_config
from route import api_route

import os

def create_app(env_name):
    # app initialization
    print('*******************Running in {} config*********************'.format(env_name))
    
    app = Flask(__name__)
    app.config.from_object(app_config[env_name])
    
    #app route
    api_route(app)

    CORS(app)

    if env_name == 'production':
        from waitress import serve
        return serve(app, host=os.environ.get('API_HOST'), port=os.environ.get('API_PORT'))
    else:
        return app