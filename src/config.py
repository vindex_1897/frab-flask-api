import os

class Development(object):
    """
    Development environment configuration
    """
    DEBUG = True
    TESTING = False
    # JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
    # SECRET_KEY=os.urandom(32)
    # WTF_CSRF_ENABLED=False
    # SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
    # FLASK_ENV = 'development'
    # SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get("SQLALCHEMY_TRACK_MODIFICATIONS")

class Production(object):
    """
    Production environment configurations
    """
    DEBUG = False
    TESTING = False
    # JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
    # SECRET_KEY=os.urandom(32)
    # WTF_CSRF_ENABLED=False
    # SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
    # FLASK_ENV = 'production'
    # SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get("SQLALCHEMY_TRACK_MODIFICATIONS")

app_config = {
    'development': Development,
    'production': Production,
}
