from flask_restful import Resource
from flask import request, make_response, jsonify

from src.form.user import UserLoginForm 
from src.service.auth_service import AuthService

class AuthController(Resource): 
    def get(self):
        return jsonify({'get': "X"})

    def post(self):
        data_validation = UserLoginForm(request.form)

        if data_validation.validate():
            #Authenticate User
            return make_response(jsonify(AuthService.generateToken(request.form, data_validation.user)), 200)
        else:
            return make_response(jsonify(data_validation.errors.items()), 422)
       
    def put(self):
        return jsonify({'put': "X"})

    def delete(self):      
        return jsonify({'delete': "X"})

