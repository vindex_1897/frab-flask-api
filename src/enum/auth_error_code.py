class AuthErrorCode:
    INVALID_TOKEN = 'Invalid Token.'
    EXPIRED_SIGNATURE = 'Expired Token.'
    DECODE_ERROR = 'Token Decoding Error.'
    INVALID_SIGNATURE = 'Invalid Token Signature.'
    INVALID_ISSUED_AT = 'Invalid Token Issued At.'
    INVALID_ALGORITHM = 'Invalid Token Algorithm'
    MISSING_REQUIRED_CLAIM = 'Missing Required Token Claim.'

