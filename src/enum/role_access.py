class RoleAccess:
    type = {
        'A' : 'admin',
        'R' : 'reviewer',
        'S' : 'submitter'
    }

