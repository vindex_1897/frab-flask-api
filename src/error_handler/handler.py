def register_app_error_handler(app):
    #Http Error Handlers
    app.register_error_handler(404, not_found_handler)
    app.register_error_handler(405, method_not_allowed_handler)
    #App Error Handlers
    app.errorhandler(AssertionError)(method_not_allowed_handler)
    return app
    
def not_found_handler(e):
    return 'Resource Not Found', 404

def method_not_allowed_handler(e):
    return 'Method Not Allowed', 405
