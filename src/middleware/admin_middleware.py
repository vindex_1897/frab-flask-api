from werkzeug.wrappers import Request, Response

import jwt
import os
from ..enum.auth_error_code import AuthErrorCode


class AdminMiddleware:

    BEARER_PREFIX = 'Bearer'

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        request = Request(environ)
        #Write only the urls that doesn't require any admin authentication
        if request.path == '/user' and request.method == 'POST':
            if 'id' in request.args and not request.args['id'].isspace():
                pass
                #Requesting Other
       
        print('admin url: path: %s, url: %s' % (request.path, request.url))

        return self.app(environ, start_response)
