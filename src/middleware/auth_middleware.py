from werkzeug.wrappers import Request, Response

import jwt
import os
from ..enum.auth_error_code import AuthErrorCode


class AuthMiddleware:

    BEARER_PREFIX = 'Bearer'

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        request = Request(environ)
       
        print('auth url: path: %s, url: %s' % (request.path, request.url))
        
        auth = request.headers.get('authorization')
        #unsecured endpoint
        if request.path == '/rides' and request.method == 'GET' and auth == os.environ.get('STATIC_DRIVER_TOKEN'):
            pass
        elif request.path == '/rides' and request.method == 'POST' and auth == os.environ.get('STATIC_RIDER_TOKEN'):
            pass
        elif request.path == '/':
            pass
        else: #secured endpoints
            response = Response(AuthErrorCode.INVALID_TOKEN, status=401)
                    
        if not (response is None):
            return response(environ, start_response)

        return self.app(environ, start_response)