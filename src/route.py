from flask_restful import Api
from flask import jsonify, Blueprint
from flask_wtf.csrf import generate_csrf
#handlers
from error_handler.handler import register_app_error_handler

#controller
from controller.rides_controller import RidesController
# from controller.auth_controller import AuthController

#middlewares
# from middleware.auth_middleware import AuthMiddleware
# from middleware.admin_middleware import AdminMiddleware

#api endpoints
def api_route(app):
    
    api = Api(app)

    # app.add_url_rule('/x/', view_func=X.as_view('X.route1'))

    # @app.route('/')
    # def handle_root_route():
    #     return  X().test_lol()

    @app.route('/')
    def root():
        return jsonify('Frab Flask-RESTFUL API Skeleton')

    # @app.after_request
    # def inject_csrf_token(response):
    #     response.headers.set('X-CSRF-Token', generate_csrf())
    #     return response

    #API Resource Declarations
    # api.add_resource(AuthController, '/authorization')
    api.add_resource(RidesController, '/rides')

    #Exception Handlers
    register_app_error_handler(app)

    #Hooking Middlewares
    # app.wsgi_app = AdminMiddleware(app.wsgi_app)
    # app.wsgi_app = AuthMiddleware(app.wsgi_app)

    return api