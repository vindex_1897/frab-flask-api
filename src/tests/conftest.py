import pytest
import os 

from src.app import create_app

@pytest.fixture
def app(mocker):
    mocker.patch("flask_sqlalchemy.SQLAlchemy.init_app", return_value=True)
    mocker.patch("flask_sqlalchemy.SQLAlchemy.create_all", return_value=True)
    mocker.patch("src.database.get_all", return_value={})
    app = create_app(os.environ.get('ENVIRONMENT'))
    return app

@pytest.fixture
def client(app):
    return app.test_client()

