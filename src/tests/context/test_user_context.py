import pytest

class TestUserContext():
    def test_root_url(self, client):
        response = client.get("/")
        assert response.status_code == 200
        
    def test_user_registration(self, client):
        data = {'email': 'neil@invento.io', 
                'first_name': 'Neil Daryl', 
                'middle_name': 'Reyes',
                'last_name': 'Sulit'
               }
               
        assert client.get('/').status_code == 200
        # app.test_client()
        # assert True
        # rv = client.post('/user',
        # data=json.dumps(data))